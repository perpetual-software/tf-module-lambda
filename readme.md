# Lambda Function Module

### Kitchen Sink Example
```hcl
module "aws_lambda_func" {
  source = "bitbucket.org/perpetual-software/tf-module-lambda"
  function_name = "function"
  source_dir = "./lambda"
  build_stage = "staging"
  api_gateway = true
  schedule = true
  schedule_expression = "rate(1 minute)"
  msk_triggers = true
  msk_cluster_arn = data.msk_cluster
  msk_topics = {
    "topic1": "TRIM_HORIZON",
    "topic2": "LATEST"
  }
  function_env_vars = [{
    variables =  {
      VAR_A = "a"
      VAR_B = "b"
    }
  }]
}
```

### API Gateway Only
```hcl
module "aws_lambda_func" {
  source = "bitbucket.org/perpetual-software/tf-module-lambda"
  function_name = "function"
  source_dir = "./lambda"
  build_stage = "staging"
  api_gateway = true
}
```

### On A Schedule
```hcl
module "aws_lambda_func" {
  source = "bitbucket.org/perpetual-software/tf-module-lambda"
  function_name = "function"
  source_dir = "./lambda"
  build_stage = "staging"
  schedule = true
  schedule_expression = "rate(1 minute)"
}
```

### Triggered By MSK
```hcl
module "aws_lambda_func" {
  source = "bitbucket.org/perpetual-software/tf-module-lambda"
  function_name = "function"
  source_dir = "./lambda"
  build_stage = "staging"
  msk_triggers = true
  msk_cluster_arn = data.msk_cluster.demo.arn
  msk_topics = {
    "topic1": "TRIM_HORIZON",
    "topic2": "LATEST"
  }
}
```