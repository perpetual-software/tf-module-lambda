variable "function_name" {
  type    = string
}

variable "function_arn" {
  type = string
}

variable "build_stage" {
  type = string
}

variable "msk_cluster_arn" {
  type = string
}

variable "topics" {
  type = object({})
}