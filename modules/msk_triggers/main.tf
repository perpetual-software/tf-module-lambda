resource "aws_lambda_event_source_mapping" "example" {
  for_each = var.topics
  event_source_arn  = var.msk_cluster_arn
  function_name     = var.function_arn
  topics            = [each.key]
  starting_position = each.value
}