variable "function_name" {
  type    = string
}

variable "function_arn" {
  type = string
}

variable "build_stage" {
  type = string
}

variable "schedule_expression" {
  type = string
  description = "How ofter to run the function"
}