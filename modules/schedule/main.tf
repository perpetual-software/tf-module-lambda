resource "aws_cloudwatch_event_rule" "rate" {
  name                = "fire-${var.build_stage}-${var.function_name}"
  description         = "Fires every ${var.schedule_expression}"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "check_foo_rate" {
  rule      = aws_cloudwatch_event_rule.rate.name
  target_id = "lambda"
  arn       = var.function_arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = var.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.rate.arn
}