output "invoke_url" {
  value = aws_api_gateway_deployment.staging_gateway.invoke_url
  depends_on = [
    aws_api_gateway_deployment.staging_gateway
  ]
}