variable "function_name" {
  type = string
}

variable "source_dir" {
  type        = string
  description = "Location of your lambda function (not zipped)"
}

variable "lambda_zip_loc" {
  type        = string
  description = "Location to store the zipped lambda. This should be .gitignored."
  default     = "./artefacts/lambda.zip"
}

variable "build_stage" {
  type        = string
  description = "staging/test/production etc.."
}

variable "function_env_vars" {
  type = list(object({
    variables = map(string)
  }))
  default = [{
    variables = {
      key1 = "value1"
      key2 = "value2"
    }
  }]
  description = "list of variables to store in the function environment"
}

variable "api_gateway" {
  type        = bool
  default     = false
  description = "should we provision an API gateway?"
}

variable "schedule" {
  type        = bool
  default     = false
  description = "should we trigger the function on a schedule?"
}

variable "schedule_expression" {
  type        = string
  default     = "rate(1 day)"
  description = "how often the function should trigger"
}

variable "msk_triggers" {
  type    = bool
  default = false
}

variable "msk_cluster_arn" {
  type    = string
  default = ""
}

variable "msk_topics" {
  type    = object({})
  default = {}
}