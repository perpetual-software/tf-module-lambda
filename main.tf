data "archive_file" "lambda_artifact" {
  type        = "zip"
  source_dir  = var.source_dir
  output_path = var.lambda_zip_loc
}

resource "aws_lambda_function" "function" {
  function_name    = "${var.build_stage}_${var.function_name}"
  filename         = data.archive_file.lambda_artifact.output_path
  source_code_hash = data.archive_file.lambda_artifact.output_base64sha256
  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.8"
  dynamic "environment" {
    for_each = var.function_env_vars
    content {
      variables = environment.value.variables
    }
  }
  role = aws_iam_role.lambda_exec.arn
}

resource "aws_iam_role" "lambda_exec" {
  name               = "${var.build_stage}_${var.function_name}_iam_for_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

module "aws_api_gateway" {
  count         = var.api_gateway == true ? 1 : 0
  source        = "./modules/api_gateway"
  build_stage   = var.build_stage
  function_arn  = aws_lambda_function.function.invoke_arn
  function_name = aws_lambda_function.function.function_name
}

module "aws_lambda_schedule" {
  count               = var.schedule == true ? 1 : 0
  source              = "./modules/schedule"
  build_stage         = var.build_stage
  function_arn        = aws_lambda_function.function.arn
  function_name       = aws_lambda_function.function.function_name
  schedule_expression = var.schedule_expression
}

module "aws_msk_triggers" {
  count           = var.msk_triggers == true ? 1 : 0
  source          = "./modules/msk_triggers"
  build_stage     = var.build_stage
  function_arn    = aws_lambda_function.function.arn
  function_name   = aws_lambda_function.function.function_name
  msk_cluster_arn = var.msk_cluster_arn
  topics          = var.msk_topics
}